//
//  View.swift
//  POC
//
//  Created by MacBook Pro on 08/02/22.
//

import UIKit

class StopView: UIView {
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ontimeLabel: UILabel!
    @IBOutlet weak var scheduleLabel: UILabel!
    @IBOutlet weak var predictedLabel: UILabel!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    func configure(stop: Stop) {
        titleLabel.text = stop.name
        ontimeLabel.text = "On time"
        scheduleLabel.text = stop.arrivalTimePlanned.timeFormatted()
        predictedLabel.text = stop.departureTimePlanned.timeFormatted()
    }
}
