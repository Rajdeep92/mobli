//
//  RouteDetailViewModel.swift
//  Mobli
//
//  Created by Rajdeep Singh on 03/02/22.
//

import Foundation

class RouteDetailViewModel {
    private var route: GetRouteModel!
    var nextStopindex = 1
    
    init(route: GetRouteModel) {
        self.route = route
    }
    
    var getRoute: GetRouteModel? {
        get {
            self.route
        }
    }
}

extension RouteDetailViewModel {
    func journeyInfo() -> String {
        return "\(route.journey.destination.name ?? "") to \(route.journey.destination.name ?? "")"
    }
    func reachedStop(name: String) {
        if let stop = route.stops().firstIndex(where: { $0.name == name }) {
            nextStopindex = stop + 1
        }
    }
    var nextStop: Stop {
        return route.stops()[nextStopindex]
    }
    
    func remainingTime(seconds: Double) -> (String, String) {
        let time: (Int,Int,Int) = (Int(seconds) / 3600, (Int(seconds) % 3600) / 60, (Int(seconds) % 3600) % 60)
        
        let format = "Mins"
        if time.0 == 0 {
            
        }
        return ("\(time.1):\(time.2)",format)
    }
}
