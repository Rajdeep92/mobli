//
//  MapViewmodel.swift
//  Mobli
//
//  Created by Rajdeep Singh on 02/02/22.
//

import Foundation
import MapboxDirections

class MapViewModel {
    
    private var route: GetRouteModel?
    
    func initialiseAuth(completion: @escaping(Bool) -> Void) {
        AuthManager.shared.initiateAuth(completion: completion)
    }
    
    func getRoute(completion: @escaping(Bool) -> Void) {
        MapViewService.getRoutes { response in
            switch response {
            case .success(let model):
                print("")
                
                completion(true)
            case .failure:
                print("")
                
                completion(false)
            }
        }
    }
}

extension MapViewModel {
    func stops() -> [Stop] {
        guard let route = route else {
            return []
        }
        return route.journey.legs.first?.stops ?? []
    }
    
//    func getNextStop() -> Stop {
//
//    }
//
//    func previousStop() -> Stop {
//
//    }
}
