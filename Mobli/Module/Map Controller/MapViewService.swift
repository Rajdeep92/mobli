//
//  MapViewService.swift
//  Mobli
//
//  Created by Rajdeep Singh on 08/02/22.
//

import Foundation
import Alamofire

class MapViewService {
    static func getRoutes(completion:@escaping (Result<GetRouteModel, AFError>) -> Void) {
        APIClient.performRequest(route: .getRoute, decoder: JSONDecoder(), completion: completion)
    }
}
