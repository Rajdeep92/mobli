//
//  HomeService.swift
//  Mobli
//
//  Created by MacBook Pro on 15/02/22.
//

import Foundation
import Alamofire

class HomeService {
    static func vehiclePositionUpdate(lat: Double,long: Double, completion:@escaping (Result<GetRouteModel, AFError>) -> Void) {
        APIClient.performRequest(route: .vehiclePositionUpdate(lat: lat, long: long), decoder: JSONDecoder(), completion: completion)
    }
}
