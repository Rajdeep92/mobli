//
//  ViewController.swift
//  Mobli
//
//  Created by Rajdeep Singh on 02/02/22.
//

import UIKit
import MapboxDirections
import MapboxCoreNavigation
import MapboxNavigation
import CoreLocation
import MapboxMaps

class ViewController: UIViewController {
    internal var cameraLocationConsumer: CameraLocationConsumer!
    @IBOutlet var mapViewContainer: UIView!
    @IBOutlet weak var routDetailContainer: UIView!
    let navigationLocationProvider = AppleLocationProvider()
    var routeDetailController: RouteDetailViewController!
    var viewModel: HomeViewModel = HomeViewModel()
    var timer: DispatchSourceTimer?
    var isLastStop = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.initialiseAuth { success in
            if success {
                self.viewModel.getRoute { success in
                    self.initialiseMapViewController(stops: self.viewModel.stops())
                }
            }
        }
        designMethods()
    }
   
    private func designMethods(){
//        routDetailContainer.cornerRadius = 20
        routDetailContainer.dropShadow(color: .black, opacity: 1, offSet: CGSize(width: -1, height: 2), radius: 10, scale: true)
     
    }
    
    private func initialiseMapViewController(stops: [Stop]) {
        
        navigationLocationProvider.requestTemporaryFullAccuracyAuthorization(withPurposeKey: "customKey")
        navigationLocationProvider.setDelegate(self)
        var locationOption = LocationOptions.init()
        locationOption.distanceFilter = 50.0
        navigationLocationProvider.locationProviderOptions = locationOption
        
        // Define two waypoints to travel between
        
        var waypoints = [Waypoint]()
        
        for stop in stops {
            let point = Waypoint(coordinate: CLLocationCoordinate2D(latitude: stop.latLng.lat, longitude: stop.latLng.lng), name: stop.name)
            waypoints.append(point)
        }
        // Set options
        let routeOptions = NavigationRouteOptions(waypoints: waypoints)
        
        let navigationOptions = NavigationOptions(styles: [CustomStyle()])
        
        // Request a route using MapboxDirections.swift
        Directions.shared.calculate(routeOptions) { [weak self] (session, result) in
            switch result {
            case .failure(let error):
                print(error.localizedDescription)
            case .success(let response):
                guard let self = self else { return }
                // Pass the first generated route to the the NavigationViewController
                
                let navViewController = NavigationViewController(for: response, routeIndex: 0, routeOptions: routeOptions, navigationOptions: navigationOptions)
                
                var annotations = [PointAnnotation]()
                for stop in stops {
                    var customPointAnnotation = PointAnnotation(coordinate: CLLocationCoordinate2D(latitude: stop.latLng.lat, longitude: stop.latLng.lng))
                    customPointAnnotation.image = .init(image: UIImage(named: "marker")!, name: "red_pin")
                    annotations.append(customPointAnnotation)
                }
                
                self.cameraLocationConsumer = CameraLocationConsumer(mapView: navViewController.navigationMapView?.mapView)
                
                let pointAnnotationManager =  navViewController.navigationMapView?.mapView.annotations.makePointAnnotationManager()
                pointAnnotationManager?.annotations = annotations
                navViewController.delegate = self
                
                navViewController.navigationMapView?.mapView.mapboxMap.onNext(.mapLoaded) { _ in
                    // Register the location consumer with the map
                    // Note that the location manager holds weak references to consumers, which should be retained
                    navViewController.navigationMapView?.mapView.location.addLocationConsumer(newConsumer: self.cameraLocationConsumer)
                }
                self.configureChildViewController(childController: navViewController, onView: self.mapViewContainer)
                
                self.addRouteDetailController()
                
            }
        }
    }
    
    func addRouteDetailController() {
        if let route = viewModel.getRoute() {
            routeDetailController = instance(storyboardId: "RouteDetail", controller: RouteDetailViewController.self)
            routeDetailController.viewModel = RouteDetailViewModel(route: route)
            self.configureChildViewController(childController: routeDetailController, onView: routDetailContainer)
        }
    }
}

extension ViewController: LocationProviderDelegate {
    func locationProvider(_ provider: LocationProvider, didUpdateLocations locations: [CLLocation]) {
        
    }
    
    /// Notifies the delegate with the new heading data.
    /// - Parameters:
    ///   - provider: The location provider reporting the update.
    ///   - newHeading: The new heading update.
    func locationProvider(_ provider: LocationProvider, didUpdateHeading newHeading: CLHeading) {
        
    }
    
    /// Notifies the delegate that the location provider was unable to retrieve
    /// location updates.
    /// - Parameters:
    ///   - provider: The location provider reporting the error.
    ///   - error: An error object containing the error code that indicates
    ///            why the location provider failed.
    func locationProvider(_ provider: LocationProvider, didFailWithError error: Error) {
        
    }
    
    /// Notifies the delegate that the location provider changed accuracy authorization
    /// - Parameters:
    ///   - provider: The location provider reporting the error.
    ///   - manager: The location manager that is reporting the change
    func locationProviderDidChangeAuthorization(_ provider: LocationProvider) {
        print("Location Auth")
    }
    
}


extension UIViewController {
    
    func instance<T: UIViewController>(storyboardId: String, controller: T.Type) -> T {
        let controllerID = String(describing: controller)
        let container = UIStoryboard.init(name: storyboardId, bundle: nil)
        let vc = container.instantiateViewController(withIdentifier: controllerID) as! T
        return vc
    }
    
    func configureChildViewController(childController: UIViewController, onView: UIView?) {
        var holderView = self.view
        if let onView = onView {
            holderView = onView
        }
        
        guard let holderView = holderView else {
            return
        }
        addChild(childController)
        holderView.addSubview(childController.view)
        constrainViewEqual(holderView: holderView, view: childController.view)
        childController.didMove(toParent: self)
        childController.willMove(toParent: self)
    }
    
    
    func constrainViewEqual(holderView: UIView, view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        //pin 100 points from the top of the super
        let pinTop = NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal,
                                        toItem: holderView, attribute: .top, multiplier: 1.0, constant: 0)
        let pinBottom = NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal,
                                           toItem: holderView, attribute: .bottom, multiplier: 1.0, constant: 0)
        let pinLeft = NSLayoutConstraint(item: view, attribute: .left, relatedBy: .equal,
                                         toItem: holderView, attribute: .left, multiplier: 1.0, constant: 0)
        let pinRight = NSLayoutConstraint(item: view, attribute: .right, relatedBy: .equal,
                                          toItem: holderView, attribute: .right, multiplier: 1.0, constant: 0)
        
        holderView.addConstraints([pinTop, pinBottom, pinLeft, pinRight])
    }
    
    func showAlert(withTitle title: String, message: String, completion: (() -> Void)? = nil){
        let okAction = UIAlertAction(title: "Ok", style: .default){ (action) in
            completion?()
        }
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert);
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}


class CustomStyle: DayStyle {
    required init() {
        super.init()
        styleType = .day
        statusBarStyle = .default
    }
    
    override func apply() {
        super.apply()
        BottomBannerView.appearance().backgroundColor = .orange
        BottomBannerView.appearance().isHidden = true
        TopBannerView.appearance().isHidden = true
    }
}


extension ViewController: NavigationViewControllerDelegate {
    
    func navigationViewController(_ navigationViewController: NavigationViewController, willArriveAt waypoint: Waypoint, after remainingTimeInterval: TimeInterval, distance: CLLocationDistance) {
        routeDetailController.willArriveAt(stopName: waypoint.name ?? "", after: remainingTimeInterval, distance: distance)
    }
    
    func navigationViewController(_ navigationViewController: NavigationViewController, didUpdate progress: RouteProgress, with location: CLLocation, rawLocation: CLLocation) {
        print(location.coordinate.longitude)
        isLastStop = progress.isFinalLeg
       
        
    }
    
    func navigationViewController(_ navigationViewController: NavigationViewController, didArriveAt waypoint: Waypoint) -> Bool {
        print("Route")
        // Get the
        routeDetailController.reachedStop(stopName: waypoint.name ?? "")
        if isLastStop {
            self.showAlert(withTitle: "Congratulations", message: "Your Destination has been reached") {
                self.stopTimer()
            }
            return false
        } else {
            self.startTimer(lat: waypoint.coordinate.latitude, long: waypoint.coordinate.longitude)
            return true
        }
    }
       
}

extension ViewController{
    
    func startTimer(lat: CLLocationDegrees, long: CLLocationDegrees) {
        let queue = DispatchQueue(label: "com.mobli.app.stopsUpdate")
        timer = DispatchSource.makeTimerSource(queue: queue)
        timer!.schedule(deadline: .now(), repeating: .seconds(20))
        timer!.setEventHandler { [weak self] in
            self?.viewModel.apiCallToVehiclePositionUpdate(lat: Double(lat), long: Double(long)) { success in
                if success{
                    debugPrint("Success")
                }
            }
        }
        timer!.resume()
    }
    
    func stopTimer() {
        timer = nil
    }
}

public class CameraLocationConsumer: LocationConsumer {
    weak var mapView: MapView?
    
    init(mapView: MapView?) {
        self.mapView = mapView
    }
    
    public func locationUpdate(newLocation: Location) {
        print("\(newLocation)")
    }
}

