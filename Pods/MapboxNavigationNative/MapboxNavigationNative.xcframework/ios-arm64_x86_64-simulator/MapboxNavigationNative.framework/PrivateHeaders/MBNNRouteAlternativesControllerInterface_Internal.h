// This file is generated and will be overwritten automatically.

#import <Foundation/Foundation.h>
#import <MapboxNavigationNative/MBNNActiveGuidanceGeometryEncoding.h>
#import <MapboxNavigationNative/MBNNRefreshAlternativesCallback_Internal.h>

@class MBNNRouteAlternativesOptions;
@class MBNNRoutingProfile;
@protocol MBNNRouteAlternativesObserver;

NS_SWIFT_NAME(RouteAlternativesControllerInterface)
@protocol MBNNRouteAlternativesControllerInterface
/**
 * Enables requesting alternate routes as soon as we pass a fork with an alternate route.
 *  Passed altrenal route will disappear from the list of alternative routes at any value.
 *  Enabled by default.
 */
- (void)enableRequestAfterForkForEnable:(BOOL)enable;
/**
 * Enables periodic polling alternative routes while no one alternative route exist.
 *  This will help avoid a situation when server has not returned any alternatives, but may return them later during the ride.
 *  Refreshes will be stopped if status.routeState is Invalid, OffRoute or Complete. And will be resumed otherwise.
 *  Now it can consume a lot of traffic!
 *  Disabled by default.
 */
- (void)enableOnEmptyAlternativesRequestForEnable:(BOOL)enable;
/**
 * Set default `RoutingProfile` (Driving, Walking, Cycling, DrivingTraffic).
 *  Default profile will be used if no `routeRequest` was set to `navigator.setRoute`.
 */
- (void)setDefaultRoutingProfileForProfile:(nonnull MBNNRoutingProfile *)profile;
/**
 * Set default `ActiveGuidanceGeometryEncoding` (GeoJSON, Polyline5, Polyline6).
 *  Default geometry encofing will be used if no `routeRequest` was seted to `navigator.setRoute`.
 */
- (void)setDefaultGeometryEncodingForGeoEncofing:(MBNNActiveGuidanceGeometryEncoding)geoEncofing;
/** Set RouteAlternativesOptions. */
- (void)setRouteAlternativesOptionsForOptions:(nonnull MBNNRouteAlternativesOptions *)options;
/** Add observer. Requests will not be sent if no one observer present. */
- (void)addObserverForObserver:(nonnull id<MBNNRouteAlternativesObserver>)observer;
/** Remove observer. Requests will not be sent if no one observer present. */
- (void)removeObserverForObserver:(nonnull id<MBNNRouteAlternativesObserver>)observer;
/** Remove all observers. Requests will not be sent if no one observer present. */
- (void)removeAllObservers;
/** Make alternative route request immediately. */
- (void)refreshImmediately;
- (void)refreshImmediatelyForCallback:(nonnull MBNNRefreshAlternativesCallback)callback;
@end
