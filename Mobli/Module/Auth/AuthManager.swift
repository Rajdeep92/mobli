//
//  AuthManager.swift
//  Mobli
//
//  Created by Rajdeep Singh on 02/02/22.
//

import Foundation
import Auth0
import JWTDecode


//protocol SessionManagerProtocol: AuthManagerProtocol {
//    func resetToken(completion:@escaping (Bool) -> Void)
//    func checkSessionValidity()
//}
//
//extension SessionManagerProtocol {
//    func resetToken(completion:@escaping (Bool) -> Void) {
//        initiateAuth(completion: completion)
//    }
//
//    func checkSessionValidity() {
//
//    }
//}



class AuthManager{
    static let shared = AuthManager()
    
    private init() { }
    
    let credentialsManager = CredentialsManager(authentication: Auth0.authentication())
    
    private var authToken: String?
    func initiateAuth( completion: @escaping (Bool) -> Void) {
        guard credentialsManager.hasValid() else {
            Auth0
                .webAuth()
                .scope("openid profile")
                .audience("https://auth.dev.operator.mobli.io/oauth/login")//("https://" + clientInfo.domain + "/userinfo")
                .start { result in
                    switch result {
                    case .failure(let error):
                        // Handle the error
                        print("Error: \(error)")
                        completion(false)
                    case .success(let credentials):
                        // Do something with credentials e.g.: save them.
                        // Auth0 will automatically dismiss the login page
                        self.handleCredentials(credentials)
                        completion(true)
                    }
            }
            return
        }
        
        credentialsManager.credentials { error, credentials in
            guard error == nil, let credentials = credentials else {
                // Handle error
                print("Error: \(error)")
                completion(false)
                return
            }
            self.handleCredentials(credentials)
            completion(true)
        }
    }
    
    private func handleCredentials(_ creds: Credentials) {
        guard let accessToken = creds.accessToken else { return }
        authToken = accessToken
        _ = self.credentialsManager.store(credentials: creds)
    }
    func plistValues(bundle: Bundle) -> (clientId: String, domain: String)? {
        guard
            let path = bundle.path(forResource: "Auth0", ofType: "plist"),
            let values = NSDictionary(contentsOfFile: path) as? [String: Any]
            else {
                print("Missing Auth0.plist file with 'ClientId' and 'Domain' entries in main bundle!")
                return nil
        }

        guard
            let clientId = values["ClientId"] as? String,
            let domain = values["Domain"] as? String
            else {
                print("Auth0.plist file at \(path) is missing 'ClientId' and/or 'Domain' entries!")
                print("File currently has the following entries: \(values)")
                return nil
        }
        return (clientId: clientId, domain: domain)
    }
    
    func resetToken(completion:@escaping (Bool) -> Void) {
        initiateAuth(completion: completion)
    }
    
//    func ifUserSessionValid() -> Bool {
//        guard credentialsManager.hasValid() else {
//            return false
//        }
//        return true
//    }
    
    func getAuthToken() -> String? {
        return self.authToken ?? ""
    }
}
