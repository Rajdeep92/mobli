//
//  APIRouter.swift
//  GamePlan
//
//  Created by Rajdeep Singh on 30/08/21.
//

import Alamofire

enum BaseURL: String {
    case dev = "" // For handling different environments
}

enum APIRouter: URLRequestConvertible {
    case appInit
    case getRoute
    case vehiclePositionUpdate(lat: Double, long: Double)
}

extension APIRouter {
    // MARK: - Base URL
    private var baseUrl: BaseURL {
        return .dev
    }
    private var path: String {
        switch self {
        case .appInit:
            return "https://run.mocky.io/v3/5c138271-d8dd-4112-8fb4-3adb1b7f689e"
        case .getRoute:
            return "https://api.apps.dev.operator.mobli.io/v1/driver-app/transit-route/poc"
        case .vehiclePositionUpdate:
            return "https://api.apps.dev.operator.mobli.io/v1/driver-app/vehicle/position"
        }
    }
    
    // HTTP method for the endpoint
    private var method: HTTPMethod {
        switch self {
        case .appInit, .vehiclePositionUpdate:
            return .post
        case .getRoute:
            return .get
        }
    }
    
    //HTTP Headers
    private var headers: HTTPHeaders? {
        var reqHeaders = HTTPHeaders()
        reqHeaders["x-request-id"] = "2a0a991f-b194-40f4-ad1f-0171b4f675d4"
        reqHeaders["x-device-id"] = "fa57bcd0e5abd1968f316f3f9113fc28"
        reqHeaders["x-app-location-provider"] = "fused"
        reqHeaders["x-app-location-time"] = "1643314089153"
        reqHeaders["x-app-location-accuracy"] = "11.641"
        reqHeaders["x-app-speed-accuracy"] = "0.0"
        reqHeaders["x-app-bearing"] = "0.0"
        reqHeaders["x-app-bearing-accuracy"] = "0.0"
        reqHeaders["x-app-altitude"] = "56.60000228881836"
        reqHeaders["x-app-altitude-accuracy"] = "5.261806"
        reqHeaders["x-app-location"] = "-36.848250, 174.765375"
        reqHeaders["Host"] = "api.apps.dev.operator.mobli.io"
        reqHeaders["Authorization"] = "Bearer \(AuthManager.shared.getAuthToken() ?? "")"
        
        switch self {
       
        case .vehiclePositionUpdate(let lat, let long):
            reqHeaders["x-app-location"] = "\(lat), \(long)"
        default:
            print("print")
        }
        
        
        return reqHeaders
    }

    private var parameters: Parameters? {
        switch self {
        case .appInit, .getRoute, .vehiclePositionUpdate:
            return nil
        }
    }
}

extension APIRouter {
    
    func asURLRequest() throws -> URLRequest {
        
        let configuration = URLSessionConfiguration.default
        URLCache.shared.removeAllCachedResponses()
        configuration.requestCachePolicy = . reloadIgnoringLocalAndRemoteCacheData

        var urlRequest: URLRequest?
        if let encoded = path.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed), let url = URL(string: encoded) {
            let url = try url.asURL()
            urlRequest = URLRequest(url: url)
            // HTTP Method
            urlRequest?.httpMethod = method.rawValue
            urlRequest?.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
            
            urlRequest?.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData

            if let headers = headers {
                for h in headers {
                    urlRequest?.setValue(h.value, forHTTPHeaderField: h.name)
                }
            }
            
            // Parameters
            if let parameters = parameters {
                do {
                    urlRequest?.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
                } catch {
                    throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
                }
            }
        }
        
        // URL Request
        if let subUrlRequest = urlRequest {
            return subUrlRequest
        } else {
            throw AFError.parameterEncodingFailed(reason: .missingURL)
        }
    }
}


enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
    case clientID = "X-Client-Id"
    case apiKey = "X-Api-Key"
}

enum ContentType: String {
    case json = "application/json"
    case html = "text/html"
}

