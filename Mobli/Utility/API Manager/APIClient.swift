//
//  APIClient.swift
//  GamePlan
//
//  Created by Rajdeep Singh on 30/08/21.
//

import Alamofire

class APIClient {
    @discardableResult
    static func performRequest<T: Decodable>(route: APIRouter, decoder: JSONDecoder, completion:@escaping (Result<T, AFError>) -> Void) -> DataRequest {
        
        return AF.request(route).response { (response) in
            print("RESPONSE: \(response)")
            if let data = response.data {
                if let stringData = String(data: data, encoding: .utf8) {
                    print("\(route)")
                    print("\(stringData)")
                }
                switch response.response?.statusCode {
                case 200:
                    let object: T!
                    do {
                        object = try decoder.decode(T.self, from: data)
                    } catch {
                        print("\(error.localizedDescription)")
                        completion(.failure(.explicitlyCancelled))
                        return
                    }
                    completion(.success(object))
//                case 401:
//                    self?.resetToken { success in
//                        self?.performRequest(route: route, decoder: decoder, completion: completion)
//                    }
                default:
                    completion(.failure(.explicitlyCancelled))
                }
            }
        }
    }
}

