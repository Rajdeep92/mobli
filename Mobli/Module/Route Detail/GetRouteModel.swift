//
//  GetRouteModel.swift
//  Mobli
//
//  Created by Rajdeep Singh on 08/02/22.
//

import Foundation

struct GetRouteModel: Codable {
    var status: String!
    var journey: Journey!
    
    func stops() -> [Stop] {
        return journey.legs[0].stops.reversed() ?? []
    }
}

struct Journey: Codable {
    var origin: Origin!
    var destination: Destination!
    var legs: [Leg]!
}

struct Origin: Codable {
    var name: String!
    var latLng: LatLong!
}

struct Destination: Codable {
    var name: String!
    var latLng: LatLong!
}

struct LatLong: Codable {
    var lat: Double!
    var lng: Double!
}

struct Leg: Codable {
    var stops: [Stop]!
}

struct Stop: Codable {
    var stopCode: String!
    var name: String!
    var latLng: LatLong!
    var arrivalTimePlanned: String!
    var departureTimePlanned: String!
    var stopIndex: Int!
    var stopSequence: Int!    
}

