// This file is generated and will be overwritten automatically.

#import <Foundation/Foundation.h>

@protocol MBNNRerouteObserver;

NS_SWIFT_NAME(RerouteControllerInterface)
@protocol MBNNRerouteControllerInterface
- (void)addObserverForObserver:(nonnull id<MBNNRerouteObserver>)observer;
- (void)removeObserverForObserver:(nonnull id<MBNNRerouteObserver>)observer;
- (void)removeAllObservers;
- (uint64_t)rerouteForUrl:(nonnull NSString *)url;
- (void)cancelForId:(uint64_t)id;
@end
