// This file is generated and will be overwritten automatically.

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@class MBNNRerouteError;

NS_SWIFT_NAME(RerouteObserver)
@protocol MBNNRerouteObserver
- (void)onRerouteDetectedForId:(uint64_t)id
                      location:(CLLocationCoordinate2D)location;
- (void)onRerouteReceivedForId:(uint64_t)id
                 routeResponse:(nonnull NSString *)routeResponse;
- (void)onRerouteCancelledForId:(uint64_t)id;
- (void)onRerouteFailedForId:(uint64_t)id
                       error:(nonnull MBNNRerouteError *)error;
@end
