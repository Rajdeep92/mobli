// This file is generated and will be overwritten automatically.

#import <MapboxNavigationNative/MBNNNavigator.h>
#import <MapboxNavigationNative/MBNNSetRouteCallback_Internal.h>

@interface MBNNNavigator ()
- (void)setRouteForRouteResponse:(nullable NSString *)routeResponse
                           route:(uint32_t)route
                             leg:(uint32_t)leg
                    routeRequest:(nullable NSString *)routeRequest
                        callback:(nonnull MBNNSetRouteCallback)callback __attribute__((deprecated("Use overload with Route instead")));
- (void)setRoutesForRoutes:(nullable MBNNRoutes *)routes
                  callback:(nonnull MBNNSetRouteCallback)callback;
- (nonnull id<MBNNRerouteControllerInterface>)createRerouteControllerForRouter:(nonnull id<MBNNRouterInterface>)router __attribute((ns_returns_retained));
- (nonnull id<MBNNRouteAlternativesControllerInterface>)createRouteAlternativesControllerForRouter:(nonnull id<MBNNRouterInterface>)router __attribute((ns_returns_retained));
- (nonnull id<MBNNRouteAlternativesControllerInterface>)createRouteAlternativesController __attribute((ns_returns_retained));
@end
