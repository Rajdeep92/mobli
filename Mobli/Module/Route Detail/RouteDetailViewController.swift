//
//  RouteDetailViewController.swift
//  POC
//
//  Created by MacBook Pro on 09/02/22.
//

import UIKit

class RouteDetailViewController: UIViewController {
    
    var viewModel: RouteDetailViewModel!
    
    @IBOutlet weak var milesLabel: UILabel!
    @IBOutlet weak var minsBusLabel: UILabel!
    @IBOutlet weak var parkLane123Label: UILabel!
    @IBOutlet weak var nextStopLabel: UILabel!
    @IBOutlet weak var speedLabel: UILabel!
    @IBOutlet weak var laneEnforcementActiveLabel: UILabel!
    
    @IBOutlet weak var journeyInfoLabel: UILabel!
    @IBOutlet weak var journeyCode: UILabel!
    @IBOutlet weak var destinationPathImageView: UIImageView!
    
    
    @IBOutlet weak var destinationNameLabel: UILabel!
    @IBOutlet weak var sourceNameLabel: UILabel!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var stopsstackView: UIStackView!
    @IBOutlet weak var sliderViewContainer: UIView!
    
    var sliderView: SliderView!
    
    @IBAction func liveDistruptionsButtonActions(_ sender: UIButton) {
    }
    @IBAction func safetyAlertButtonActions(_ sender: UIButton) {
    }
    @IBAction func reportDamageButtonActions(_ sender: UIButton) {
    }
    @IBAction func customerSupportButtonActions(_ sender: UIButton) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView() {
        journeyInfoLabel.text = viewModel.journeyInfo()
        journeyCode.text = "XPC 199"
        destinationNameLabel.text = viewModel.getRoute?.journey.destination.name
        sourceNameLabel.text = viewModel.getRoute?.journey.origin.name
        
        if let stops = viewModel.getRoute?.stops() {
            for stop in stops {
                let midView = Bundle.main.loadNibNamed("StopView", owner:
                                                        self, options: nil)?.first as! StopView
                //midView.titleLabel.text = "\(i)"
                midView.configure(stop: stop)
                self.stopsstackView.addArrangedSubview(midView)
            }
            setUpStopsSlider(stopsCount: stops.count)
        }
        

        view.layoutIfNeeded()
    }
    
    func setUpStopsSlider(stopsCount: Int) {
        sliderView = Bundle.main.loadNibNamed("SliderView", owner:self, options: nil)?.first as! SliderView
        sliderView.frame = self.sliderViewContainer.bounds
        sliderView.interval = Float(1.0/Double(stopsCount))
        self.sliderViewContainer.addSubview(sliderView)
        self.sliderViewContainer.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi / 2))
        

    }
    
    func designMethods() {
        sliderView = Bundle.main.loadNibNamed("SliderView", owner:self, options: nil)?.first as! SliderView
        sliderView.frame = CGRect.init(x: (-self.scrollView.frame.size.height/2 ) - 150 , y: 0, width: self.scrollView.frame.size.height, height: self.scrollView.frame.size.height + 50)
        self.sliderViewContainer.addSubview(sliderView)

        for i in 1...10 {
            let midView = Bundle.main.loadNibNamed("StopView", owner:
                                                    self, options: nil)?.first as! StopView
            //midView.titleLabel.text = "\(i)"
            midView.ontimeLabel.text = "on time"
            self.stopsstackView.addArrangedSubview(midView)

        }
        
    }
}


extension RouteDetailViewController {
    
    func willArriveAt(stopName: String, after remainingTime: Double, distance: Double) {
        updateUIForNextStop(stopName: stopName, remainingTime: remainingTime, distance: distance)
    }
    
    func reachedStop(stopName: String) {
        viewModel.reachedStop(name: stopName)
        self.sliderView.moveToNextStation()
    }
    
    private func updateUIForNextStop(stopName: String, remainingTime: Double, distance: Double) {
        milesLabel.text = "\(distance.milesRepresentable())"
        minsBusLabel.text = viewModel.remainingTime(seconds: remainingTime).0
        nextStopLabel.text = stopName
    }
}
