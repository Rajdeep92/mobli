// This file is generated and will be overwritten automatically.

#import <Foundation/Foundation.h>
#import <MapboxNavigationNative/MBNNRouterOrigin.h>

NS_SWIFT_NAME(RouteObject)
__attribute__((visibility ("default")))
@interface MBNNRouteObject : NSObject

// This class provides custom init which should be called
- (nonnull instancetype)init NS_UNAVAILABLE;

// This class provides custom init which should be called
+ (nonnull instancetype)new NS_UNAVAILABLE;

- (nonnull instancetype)initWithResponseUuid:(nonnull NSString *)responseUuid
                                       index:(uint32_t)index
                                    response:(nonnull NSString *)response
                                routerOrigin:(nullable NSNumber *)routerOrigin;

/** UUID from route response object. This UUID can be used to refresh route. */
@property (nonatomic, readonly, nonnull, copy) NSString *responseUuid;

/**
 * Route index in routes array from direction's response.
 *  This index can be used to refresh route and find route object in routeResponse.routes JSON array.
 */
@property (nonatomic, readonly) uint32_t index;

/** This is full route's response but all routes changed to null except one route. */
@property (nonatomic, readonly, nonnull, copy) NSString *response;

/** Router origin from router. Describes which kind of router presents response. Empty optional if alternative from initial route. */
@property (nonatomic, readonly, nullable) NSNumber *routerOrigin;


@end
