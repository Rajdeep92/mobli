//
//  HomeViewModel.swift
//  Mobli
//
//  Created by Rajdeep Singh on 08/02/22.
//

import Foundation

class HomeViewModel {
    private var route: GetRouteModel?
    
    func initialiseAuth(completion: @escaping(Bool) -> Void) {
        AuthManager.shared.initiateAuth(completion: completion)
    }

    
    func getRoute(completion: @escaping(Bool) -> Void) {
        MapViewService.getRoutes { response in
            switch response {
            case .success(let model):
                print("")
                self.route = model
                completion(true)
            case .failure:
                print("")
                
                completion(false)
            }
        }
    }
    
    func apiCallToVehiclePositionUpdate(lat: Double, long: Double,completion: @escaping(Bool) -> Void) {
        HomeService.vehiclePositionUpdate(lat: lat, long: long){ response in
            switch response {
            case .success(let model):
                print("")
                completion(true)
            case .failure:
                print("")
                
                completion(false)
            }
        }
    }
    
}

extension HomeViewModel {
    func getRoute() -> GetRouteModel?{
        return route
    }
    func stops() -> [Stop] {
        guard let route = route else {
            return []
        }
        return route.journey.legs.first?.stops ?? []
    }
}
