// This file is generated and will be overwritten automatically.

#import <Foundation/Foundation.h>

NS_SWIFT_NAME(RouteAlternativesOptions)
__attribute__((visibility ("default")))
@interface MBNNRouteAlternativesOptions : NSObject

// This class provides custom init which should be called
- (nonnull instancetype)init NS_UNAVAILABLE;

// This class provides custom init which should be called
+ (nonnull instancetype)new NS_UNAVAILABLE;

- (nonnull instancetype)initWithRequestIntervalSeconds:(double)requestIntervalSeconds
                          minTimeBeforeManeuverSeconds:(double)minTimeBeforeManeuverSeconds;

/** Re-request alternative routes interval. 3 minutes by default. Minimum 30 seconds. */
@property (nonatomic, readonly) double requestIntervalSeconds;

/**
 * Remove routes if travel time to fork point less or equal this time.
 *  0 by default.
 */
@property (nonatomic, readonly) double minTimeBeforeManeuverSeconds;


- (BOOL)isEqualToRouteAlternativesOptions:(nonnull MBNNRouteAlternativesOptions *)other;

@end
