//
//  Extensions.swift
//  Mobli
//
//  Created by Rajdeep Singh on 13/02/22.
//

import Foundation

extension String {
    func timeFormatted() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        if let date = dateFormatter.date(from: self) {
            dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
            dateFormatter.timeZone = .current
            return dateFormatter.string(from: date)
        }
        return ""
    }
}

extension Double {
    func miles() -> Double{
        return self*0.62137/1000
    }
    
    func milesRepresentable() -> String {
        return String(format: "%.2f", miles())
    }
}
