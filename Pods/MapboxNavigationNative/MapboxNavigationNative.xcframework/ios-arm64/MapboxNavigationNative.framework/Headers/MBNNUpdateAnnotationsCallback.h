// This file is generated and will be overwritten automatically.

#import <Foundation/Foundation.h>

/**
 * Callback which is getting called to report updateAnnotations result.
 * @param   updateAnnotationsResult True if the annotations could be updated false if not (wrong number of annotations)
 */
NS_SWIFT_NAME(UpdateAnnotationsCallback)
typedef void (^MBNNUpdateAnnotationsCallback)(BOOL updateAnnotationsResult); // NOLINT(modernize-use-using)
