//
//  MapViewController.swift
//  Mobli
//
//  Created by Rajdeep Singh on 02/02/22.
//

import UIKit
import MapboxDirections
import MapboxCoreNavigation
import MapboxNavigation
import CoreLocation
import MapboxMaps

class MapViewController: NavigationViewController {

    var viewModel: MapViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
}

extension MapViewController {
}

//extension MapViewController: LocationProviderDelegate {
//    func locationProvider(_ provider: LocationProvider, didUpdateLocations locations: [CLLocation]) {
//
//    }
//
//    /// Notifies the delegate with the new heading data.
//    /// - Parameters:
//    ///   - provider: The location provider reporting the update.
//    ///   - newHeading: The new heading update.
//    func locationProvider(_ provider: LocationProvider, didUpdateHeading newHeading: CLHeading) {
//
//    }
//
//    /// Notifies the delegate that the location provider was unable to retrieve
//    /// location updates.
//    /// - Parameters:
//    ///   - provider: The location provider reporting the error.
//    ///   - error: An error object containing the error code that indicates
//    ///            why the location provider failed.
//    func locationProvider(_ provider: LocationProvider, didFailWithError error: Error) {
//
//    }
//
//    /// Notifies the delegate that the location provider changed accuracy authorization
//    /// - Parameters:
//    ///   - provider: The location provider reporting the error.
//    ///   - manager: The location manager that is reporting the change
//    func locationProviderDidChangeAuthorization(_ provider: LocationProvider) {
//
//    }
//
//}
