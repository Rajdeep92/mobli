//
//  SliderView.swift
//  POC
//
//  Created by MacBook Pro on 09/02/22.
//

import UIKit

class SliderView: UIView {
    var interval: Float = 0.0
        @IBOutlet weak var slider: UISlider!{
        didSet{
//            slider.isUserInteractionEnabled = false
            let ratio : CGFloat = CGFloat (0.7)
            let thumbImage : UIImage = UIImage(named: "sliderKnob")!
            let minImage : UIImage = UIImage(named: "sliderMinImage")!
            let maxImage : UIImage = UIImage(named: "sliderMaxImage")!
            let size = CGSize(width: thumbImage.size.width * ratio, height: thumbImage.size.height * ratio)
            slider.setThumbImage(thumbImage, for: .normal)
//            slider.minimumValueImage = minImage
//            slider.maximumValueImage = maxImage
//            
            slider.value = 0.0
        }
        
    }
    
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{ UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0); image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
        UIGraphicsEndImageContext()
        return newImage

    }
    
    func moveToNextStation() {
        slider.setValue(slider.value + interval, animated: true)
    }
}


open class CustomSlider : UISlider {
    @IBInspectable open var trackWidth:CGFloat = 2 {
        didSet {setNeedsDisplay()}
    }

    override open func trackRect(forBounds bounds: CGRect) -> CGRect {
        let defaultBounds = super.trackRect(forBounds: bounds)
        return CGRect(
            x: defaultBounds.origin.x,
            y: defaultBounds.origin.y + defaultBounds.size.height/2 - trackWidth/2,
            width: defaultBounds.size.width,
            height: trackWidth
        )
    }
}

