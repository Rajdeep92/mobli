// This file is generated and will be overwritten automatically.

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MapboxNavigationNative/MBNNCompassUpdateCallback_Internal.h>
#import <MapboxNavigationNative/MBNNIMUUpdateCallback_Internal.h>

NS_SWIFT_NAME(IMUService)
@protocol MBNNIMUService
- (int32_t)registerCompassUpdateCallbackForCallback:(nonnull MBNNCompassUpdateCallback)callback;
- (int32_t)registerIMUUpdateCallbackForCallback:(nonnull MBNNIMUUpdateCallback)callback;
- (void)unregisterCallbackForId:(int32_t)id;
- (void)updateForCoordinate:(CLLocationCoordinate2D)coordinate
                   altitude:(nullable NSNumber *)altitude;
@end
